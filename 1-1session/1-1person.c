#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Peter;
    PPerson pPe;
    pPe = &Peter;
    strcpy(pPe->name, "Peter");
    pPe->heightcm = 175;
    Peter.weightkg = 78.7;
    printf("%s's height is: %d cm; %s's weight is: %.1f kg;\n",pPe->name,pPe->heightcm,Peter.name,Peter.weightkg);

    return 0;
}
